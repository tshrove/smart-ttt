# Smart TTT

This project was a fun project I set out to do in order to learn more about deep neural networks. I trained a neural network using Keras/Tensorflow that would play Tic-Tac-Toe with a human player.

## Development

There are three projects.
1. Experimentation (This is the juypter notebook for training the model.)
2. App (This is the VueJS frontend)
3. Backend (This is the Flask Python backend API)

## App
Navigate to the /app folder in order to setup the project. Use the README.md for this folder to setup the projet.

## Backend
Navigate to the /backend folder in order to setup the project. Use the README.md for this folder to setup the projet.

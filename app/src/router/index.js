import Vue from "vue";
import VueRouter from "vue-router";
import TicTacToe from "../views/TicTacToe";
import Cell from "../components/Cell";

Vue.component("tic-tac-toe", TicTacToe);
Vue.component("cell", Cell);

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "TicTacToe",
    component: TicTacToe
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;

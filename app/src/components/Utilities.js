export default class Utilities {
    constructor() {
        this.aiMoves = 0;
        this.aiMoveHistory = [-1, -1, -1, -1, -1, -1, -1, -1];
    }

    doMove(movePos) {
        this.aiMoveHistory[this.aiMoves] = movePos;
        this.aiMoves++;
    }

    static convertGridToLoc(move) {
        if (move.x === 0 && move.y === 0) {
            return 0;
        } else if (move.x === 0 && move.y === 1) {
            return 1;
        } else if (move.x === 0 && move.y === 2) {
            return 2;
        } else if (move.x === 1 && move.y === 0) {
            return 3;
        } else if (move.x === 1 && move.y === 1) {
            return 4;
        } else if (move.x === 1 && move.y === 2) {
            return 5;
        } else if (move.x === 2 && move.y === 0) {
            return 6;
        } else if (move.x === 2 && move.y === 1) {
            return 7;
        } else if (move.x === 2 && move.y === 2) {
            return 8;
        } else {
            return -1;
        }
    }

    static convertLocToGrid(movePos) {
        switch(movePos) {
            case 0:
                return { x: 0, y: 0 };
            case 1:
                return { x: 0, y: 1 };
            case 2:
                return { x: 0, y: 2 };
            case 3:
                return { x: 1, y: 0 };
            case 4:
                return { x: 1, y: 1 };
            case 5:
                return { x: 1, y: 2 };
            case 6:
                return { x: 2, y: 0 };
            case 7:
                return { x: 2, y: 1 };
            case 8:
                return { x: 2, y: 2 };
        }
    }
}
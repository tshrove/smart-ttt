from flask import Flask, jsonify
from flask import request
import tensorflow as tf
from flask_cors import CORS

app = Flask(__name__)
# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

model = tf.keras.models.load_model('model_v2.h5')


def cal_next_move(pred_vals):
    _preditems = []
    for i, item in enumerate(pred_vals):
        _preditems.append({'idx': i, 'val': str(item)})
    _preditems.sort(key=lambda x: x['val'], reverse=True)
    return _preditems


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/nextmove', methods=['POST'])
def next_move():
    try:
        data = request.get_json()
        _pred = model.predict([data['moves']])
        _next = cal_next_move(_pred[0])
        return jsonify({"predictions": _next})
    except e:
        print(e)


if __name__ == '__main__':
    app.run()
